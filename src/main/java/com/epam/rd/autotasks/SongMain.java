package com.epam.rd.autotasks;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * The main entry point of the Song application.
 */
@ComponentScan(basePackages = "com.epam.rd.autotasks")
public class SongMain {
    public static void main(String[] args) {
        // Create a Spring application context based on the configuration in SongMain class.
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SongMain.class)) {
            // Retrieve the Song bean from the context.
            Song song = context.getBean(Song.class);

            // Print the details of the Song bean.
            System.out.println(song);
        }
    }
}

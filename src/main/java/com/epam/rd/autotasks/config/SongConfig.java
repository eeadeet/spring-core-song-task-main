package com.epam.rd.autotasks.config;

import com.epam.rd.autotasks.Song;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuration class for creating a Song bean and specifying the properties file location.
 */
@Configuration
@PropertySource("classpath:/application.properties")
public class SongConfig {

    /**
     * Creates a Song bean with properties initialized from the specified properties file.
     *
     * @return A Song bean configured with properties from the application.properties file.
     */
    @Bean
    public Song song() {
        return new Song();
    }
}

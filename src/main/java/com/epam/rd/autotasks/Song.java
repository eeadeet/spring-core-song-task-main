package com.epam.rd.autotasks;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Represents a song with attributes such as title, artist, and year.
 */
@Component
public class Song {
    @Value("${song.title}")
    private String title;

    @Value("${song.artist}")
    private String artist;

    @Value("${song.year}")
    private String year;

    /**
     * Constructs a Song with specified title, artist, and year.
     *
     * @param title  The title of the song.
     * @param artist The artist of the song.
     * @param year   The year the song was released.
     */
    public Song(String title, String artist, String year) {
        this.title = title;
        this.artist = artist;
        this.year = year;
    }

    /**
     * Default constructor.
     */
    public Song() {
    }

    /**
     * Gets the title of the song.
     *
     * @return The title of the song.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the song.
     *
     * @param title The title of the song.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the artist of the song.
     *
     * @return The artist of the song.
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets the artist of the song.
     *
     * @param artist The artist of the song.
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Gets the year the song was released.
     *
     * @return The year the song was released.
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the year the song was released.
     *
     * @param year The year the song was released.
     */
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Song {" +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
